package com.example.clevertask5.retrofit;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitCreator {

    public static BankPointsApiService createApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://belarusbank.by/api/")
                .client(createOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(creatwGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(BankPointsApiService.class);
    }


    private static Gson creatwGson() {
        return new GsonBuilder().create();
    }

    private static OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }
}
