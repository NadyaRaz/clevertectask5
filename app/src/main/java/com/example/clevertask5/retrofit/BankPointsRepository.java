package com.example.clevertask5.retrofit;

import com.example.clevertask5.dto.BankPointDto;
import com.example.clevertask5.dto.BankPointModel;
import com.example.clevertask5.dto.FilialPointDto;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

public class BankPointsRepository {

    private BankPointsApiService apiService;

    public BankPointsRepository(BankPointsApiService apiService) {
        this.apiService = apiService;
    }

    public Single<List<BankPointModel>> getAtm(String city) {
        return apiService.getATM(city).map(points -> {
                    ArrayList<BankPointModel> models = new ArrayList<>();
                    for (BankPointDto point : points) {
                        models.add(
                                new BankPointModel(
                                        point.getLatitude(),
                                        point.getLongitude(),
                                        "ATM / " + point.getWorkTime()
                                )
                        );
                    }
                    return models;
                }
        );
    }

    public Single<List<BankPointModel>> getInfobox(String city) {
        return apiService.getInfobox(city).map(points -> {
                    ArrayList<BankPointModel> models = new ArrayList<>();
                    for (BankPointDto point : points) {
                        models.add(
                                new BankPointModel(
                                        point.getLatitude(),
                                        point.getLongitude(),
                                        "Infobox / " + point.getWorkTime()
                                )
                        );
                    }
                    return models;
                }
        );
    }

    public Single<List<BankPointModel>> getFilials(String city) {
        return apiService.getFilials(city).map(points -> {
                    ArrayList<BankPointModel> models = new ArrayList<>();
                    for (FilialPointDto point : points) {
                        models.add(
                                new BankPointModel(
                                        point.getLatitude(),
                                        point.getLongitude(),
                                        "Filial / " + point.getWorkTime()
                                )
                        );
                    }
                    return models;
                }
        );
    }
}
