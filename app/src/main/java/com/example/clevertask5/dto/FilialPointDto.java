package com.example.clevertask5.dto;

import com.google.gson.annotations.SerializedName;

public class FilialPointDto {

    @SerializedName("GPS_X")
    private String latitude;

    @SerializedName("GPS_Y")
    private String longitude;

    @SerializedName("info_worktime")
    private String workTime;

    public FilialPointDto(String latitude, String longitude, String workTime) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.workTime = workTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

}
