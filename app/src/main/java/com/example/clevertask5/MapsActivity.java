package com.example.clevertask5;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.example.clevertask5.dto.BankPointModel;
import com.example.clevertask5.retrofit.BankPointsApiService;
import com.example.clevertask5.retrofit.BankPointsRepository;
import com.example.clevertask5.retrofit.RetrofitCreator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private final int ZOOM_LEVEL_CITY = 10;
    private final int ZOOM_LEVEL_STREET = 15;
    private final LatLng DEFAULT_COORDINATES = new LatLng(52.425163, 31.015039);
    private final int LOCATION_PERMISSION_CODE = 1;
    private LatLng currentLocation = null;
    private GoogleMap mMap;

    private BankPointsApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        apiService = RetrofitCreator.createApiService();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_COORDINATES, ZOOM_LEVEL_CITY));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        } else {
            populateMap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    populateMap();
                }
                break;
            }
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @SuppressLint("MissingPermission") //запрашиваем выше
    private void populateMap() {
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, ZOOM_LEVEL_STREET));
        } catch (NullPointerException exception) {
            Log.d("CurrentLocationExc", exception.getStackTrace().toString());
        }
        loadAtmList("Гомель");
    }

    private void loadAtmList(String city) {
        BankPointsRepository repository = new BankPointsRepository(apiService);
        Single.zip(
                repository.getAtm(city), repository.getFilials(city), repository.getInfobox(city),
                (atms, filials, inboxes) -> {
                    ArrayList<BankPointModel> allModels = new ArrayList<>();
                    allModels.addAll(atms);
                    allModels.addAll(filials);
                    allModels.addAll(inboxes);
                    allModels.sort((point1, point2) -> Double.compare(getDistance(point1), getDistance(point2)));
                    return allModels.subList(0, 10);
                }
        )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        atms -> {
                            for (BankPointModel atm : atms) {
                                mMap.addMarker(
                                        new MarkerOptions()
                                                .position(new LatLng(Double.valueOf(atm.getLatitude()), Double.valueOf(atm.getLongitude())))
                                                .title(atm.getInfo())
                                );
                            }
                        },
                        error -> {
                            Toast.makeText(getApplicationContext(), "Произошла ошибка во время загрузки данных", Toast.LENGTH_SHORT).show();
                        }
                );
    }

    private double getDistance(BankPointModel point) {
        LatLng position;
        if (currentLocation != null) {
            position = currentLocation;
        } else {
            position = DEFAULT_COORDINATES;
        }
        return Math.sqrt(Math.pow(position.latitude - Double.parseDouble(point.getLatitude()), 2) +
                Math.pow(position.longitude - Double.parseDouble(point.getLongitude()), 2));
    }
}
